﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpheliasOasis.Models;

namespace OpheliasOasis.ViewModels
{
    public class IncentiveReportPageViewModel
    {
        // Employee Attributes
        public int empID { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public bool isManager { get; set; }
        public string password { get; set; }
        public List<employee> employees { get; set; }

        // guest.cs Attributes
        public int gID { get; set; }
        public string email { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }

        // ReservationType.cs Attributes
        public int resTypeId { get; set; }
        public string resType { get; set; }

        //reservation.cs Attributes
        public int ID { get; set; }
        public int guestID { get; set; }
        public Nullable<System.DateTime> arrivalDate { get; set; }
        public Nullable<System.DateTime> leaveDate { get; set; }
        public Nullable<System.DateTime> checkInDate { get; set; }
        public Nullable<System.DateTime> checkOutDate { get; set; }
        public Nullable<int> room { get; set; }
        public bool noShow { get; set; }
        public bool isCancelled { get; set; }
        public Nullable<System.DateTime> paidDate { get; set; }
        public Nullable<double> prepaidCharge { get; set; }
        public int typeID { get; set; }


        // Custom Attributes
        //public System.Web.Mvc.SelectList resListItems { get; set; }
        //public System.Web.Mvc.SelectList roomListItems { get; set; }
    }
}