﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpheliasOasis.Models;

namespace OpheliasOasis.ViewModels
{
    public class LoginPageViewModel
    {
        // Employee Attributes
        public int empID { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public bool isManager { get; set; }
        public string password { get; set; }
        public List<employee> employees { get; set; }
        public System.Web.Mvc.SelectList employeeSelect { get; set; }
        public string message { get; set;}
        public string messageColor { get; set; }

        public const string START_TEXT = "Please log in.";
        public const string FAIL_TEXT = "Wrong password.";
    }
}