﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpheliasOasis.Models;

namespace OpheliasOasis.ViewModels
{
    public class RatesPageViewModel
    {

        // baseRates.cs Attributes
        public double rate { get; set; }
        public System.DateTime rateDate { get; set; }


        // Custom Attributes

    }
}