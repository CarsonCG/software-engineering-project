﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpheliasOasis.Models;

namespace OpheliasOasis.ViewModels
{
    public class CreditCardPageViewModel
    {

        // creditCard.cs Attributes
        public int cID { get; set; }
        public int guestID { get; set; }
        public string number { get; set; }
        public string name { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public int securityCode { get; set; }

        // guest.cs Attributes
        public int gID { get; set; }
        public string email { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }


        // Custom Attributes
        //public System.Web.Mvc.SelectList guestList { get; set; }
    }
}