﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OpheliasOasis.Models;

/*
In addition, the product must print accommodation bills.A bill must reflect the date
the bill was printed, the guest name, room number, arrival date, departure date, number of
nights, and total charge.For prepaid and 60-day reservations, the bill must reflect the date on
which it was paid in advance and the amount paid.Accommodation bills are handed to the
guests when they check out.
*/

namespace OpheliasOasis.ViewModels
{
    public class PrintBillPageViewModel
    {
        // select a guest to print
        public guest currentGuest { get; set; }
        public List<guest> guests { get; set; }
        public System.Web.Mvc.SelectList guestSelect { get; set; }

        public Nullable<System.DateTime> printDate { get; set; }
        public int guestID { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string email { get; set; }
        public Nullable<int> room { get; set; }
        public int resID { get; set; }
        public int typeID { get; set; }
        public string resType { get; set; }
        public Nullable<System.DateTime> arrivalDate { get; set; }
        public Nullable<System.DateTime> leaveDate { get; set; }

        // calculate number of nights
        public Nullable<System.DateTime> checkInDate { get; set; }
        public Nullable<System.DateTime> checkOutDate { get; set; }

        public Nullable<System.DateTime> paidDate { get; set; }
        public Nullable<double> prepaidCharge { get; set; }
        public Nullable<double> totalCharge { get; set; }
        public double charge { get; set; }

        public List<reservation> guestReservations { get; set; }
        public System.Web.Mvc.SelectList resListItems { get; set; }
        public System.Web.Mvc.SelectList CurrentReserve { get; set; }
        public List<reservationDay> resDayList { get; set; }
        public double multiplier { get; set; }
        public double sumPrice { get; set; }

    }
}