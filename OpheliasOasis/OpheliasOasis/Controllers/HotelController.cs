﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OpheliasOasis.Models;
using OpheliasOasis.ViewModels;

namespace OpheliasOasis.Controllers
{
	public class HotelController : Controller
	{
		HotelDBEntitiesMDF db = new HotelDBEntitiesMDF(); // Database connection db

		// GET: Hotel
		public ActionResult Index()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "Index";
                return RedirectToAction("Login");
            }


            return View();
		}
		
		// GET: Hotel/Reservation
		public ActionResult Reservation()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "Reservation";
                return RedirectToAction("Login");
            }
            ReservationPageViewModel resVM = new ReservationPageViewModel();

			// Setup SelectList for DropDownBox
			List<reservation> currentReserve = db.reservations.ToList();
			resVM.CurrentReserve = new SelectList(currentReserve, "ID", "ID");

			return View(resVM);
		}

		public ActionResult EditRecord(ReservationPageViewModel model, IEnumerable<bool> noShow, IEnumerable<bool> isCancelled)
		{

			if (noShow != null && noShow.Count() == 2)
			{
				model.noShow = true;
			}
			else
			{
				model.noShow = false;
			}

			if (isCancelled != null && isCancelled.Count() == 2)
			{
				model.isCancelled = true;
			}
			else
			{
				model.isCancelled= false;
			}

			try
			{

				reservation resData = db.reservations.SingleOrDefault(x => x.ID == model.ID);
				guest guestData = db.guests.SingleOrDefault(y => y.ID == resData.guestID);

  
				guestData.firstName = model.firstName;
				guestData.lastName = model.lastName;
				resData.checkInDate = model.checkInDate;
				resData.checkOutDate = model.checkOutDate;
				guestData.email = model.email;
				resData.arrivalDate = model.arrivalDate;
				resData.leaveDate = model.leaveDate;
				resData.room = model.room;
				resData.isCancelled = model.isCancelled;
				resData.paidDate = model.paidDate;
				resData.noShow = model.noShow;

				db.SaveChanges();

			}
			catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("Index");
		}

		// GET: Hotel/Reservation
		public ActionResult Login()
		{
			LoginPageViewModel loginVM = new LoginPageViewModel();

			if (TempData["LoginView"] != null)
			{
				// Preserves view from 
				loginVM = (LoginPageViewModel)TempData["LoginView"];

				List<employee> userList = db.employees.ToList();
				loginVM.employees = userList;
				loginVM.employeeSelect = new SelectList(userList, "ID", "name");
			}
			else
			{
				// Setup SelectList for DropDownBox
				List<employee> userList = db.employees.ToList();
				loginVM.employees = userList;
				loginVM.employeeSelect = new SelectList(userList, "ID", "name");
				loginVM.message = LoginPageViewModel.START_TEXT;
				loginVM.messageColor = "#000000";
			}


			return View(loginVM);
		}

		public ActionResult LoginUser(LoginPageViewModel model)
		{
			string password = model.password;
			int empID = model.empID;

			List<employee> userList = db.employees.ToList();
			foreach (employee e in userList)
			{
				if (e.ID == empID && e.password == password)
				{
                    this.Session["CurrentUser"] = e;
                    if (TempData["LoginRedirect"] == null)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction((string)TempData["LoginRedirect"]);
                    }
				} 
			}

			model.message = LoginPageViewModel.FAIL_TEXT;
			model.messageColor = "#FF0000";

			TempData["LoginView"] = model;

			return RedirectToAction("Login");
		}

        public ActionResult GenerateEmail()
        {
            EmailPageViewModel emailVM = new EmailPageViewModel();

            return View(emailVM);
        }

		public ActionResult SaveRecord(ReservationPageViewModel model)
		{
			try
			{

				reservation resData = new reservation();
				guest guestData = new guest();

				guestData.firstName = model.firstName;
				guestData.lastName = model.lastName;
				resData.checkInDate = model.checkInDate;
				resData.checkOutDate = model.checkOutDate;
				guestData.email = model.email;
				resData.typeID = model.typeID;

				db.guests.Add(guestData);
				db.reservations.Add(resData);


				db.SaveChanges();

				guestData = db.guests.SingleOrDefault(y => y.lastName == model.lastName && y.firstName == model.firstName && y.email == model.email);
				resData = db.reservations.SingleOrDefault(x => x.checkInDate == model.checkInDate && x.guestID == guestData.ID);
				var resID = resData.ID;

				db.createEachReservationDay(model.checkInDate, model.checkOutDate, resID);
			}
			catch(Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("Index");
		}

		// GET: Hotel/ExpectedOccupancyReport
		public ActionResult ExpectedOccupancyReport()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "ExpectedOccupancyReport";
                return RedirectToAction("Login");
            }
            try
			{
				var context = new HotelDBEntitiesMDF();
				var result = context.getExpectedOccupancy();
				return View(result.ToList());
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		// GET: Hotel/ExpectedRoomIncomeReport
		public ActionResult ExpectedRoomIncomeReport()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "ExpectedRoomIncomeReport";
                return RedirectToAction("Login");
            }
            try
			{
				var context = new HotelDBEntitiesMDF();
				var result = context.getExpectedIncome();
				return View(result.ToList());
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		// GET: Hotel/IncentiveReport
		public ActionResult IncentiveReport()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "IncentiveReport";
                return RedirectToAction("Login");
            }
            try
			{
				var context = new HotelDBEntitiesMDF();
				var result = context.getIncentiveReport();
				return View(result.ToList());
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		// GET: Hotel/DailyArrivalsReport
		public ActionResult DailyArrivalsReport()
        {
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "DailyArrivalsReport";
                return RedirectToAction("Login");
            }
            try { 
			List<reservation> resList = db.reservations.ToList();
			var today = DateTime.Today;

			List<DailyArrivalsReportPageViewModel> daReportVMResList = resList.Join(
				db.guests,
				res => res.guestID,
				gue => gue.ID,
				(res,gue) => new {res,gue})
				.Join(db.ReservationTypes,
					o => o.res.typeID,
					type => type.resTypeId,
					(res2,type) => new{res2, type})
				.Where(daReportVM => (daReportVM.res2.res.isCancelled == false)
				&& (daReportVM.res2.res.noShow == false)
				&& (daReportVM.res2.res.checkInDate == today))
				.OrderBy(daReportVM => daReportVM.res2.gue.lastName)
				.ThenBy(daReportVM => daReportVM.res2.gue.firstName)
				.Select(daReportVM => new DailyArrivalsReportPageViewModel
				{
					firstName = daReportVM.res2.gue.firstName,
					lastName = daReportVM.res2.gue.lastName,
					room = daReportVM.res2.res.room,
					resType = daReportVM.type.resType,
					checkOutDate = daReportVM.res2.res.checkOutDate
				}).ToList();

				return View(daReportVMResList);
			}
			catch (Exception ex)
			{
				throw ex;
			} 

		}

		// GET: Hotel/DailyOccupancyReport
		public ActionResult DailyOccupancyReport()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "DailyOccupancyReport";
                return RedirectToAction("Login");
            }
            try { 
			List<reservation> resList = db.reservations.ToList();
			var today = DateTime.Today;

			List<DailyOccupancyReportPageViewModel> doReportVMResList = resList.Join(
				db.guests,
				res => res.guestID,
				gue => gue.ID,
				(res, gue) => new { res, gue })
				.Where(doReportVM => (doReportVM.res.checkInDate < today)
					&& (doReportVM.res.checkOutDate >= today)
					&& (doReportVM.res.isCancelled == false)
					&& (doReportVM.res.noShow == false))
				.OrderBy(doReportVM => doReportVM.res.room)
				.Select(doReportVM => new DailyOccupancyReportPageViewModel
				{
					firstName = doReportVM.gue.firstName,
					lastName = doReportVM.gue.lastName,
					room = doReportVM.res.room,
					checkOutDate = doReportVM.res.checkOutDate
				}).ToList();

				return View(doReportVMResList);
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		[HttpGet]
		public ActionResult ReservationForm()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "ReservationForm";
                return RedirectToAction("Login");
            }
            ReservationPageViewModel resVM = new ReservationPageViewModel();

			// Setup SelectList for DropDownBox
			List<ReservationType> resList = db.ReservationTypes.ToList();
			resVM.resListItems = new SelectList(resList, "resTypeId", "resType");

			

			return PartialView("_ReservationForm", resVM);
		}

		[HttpPost]
		public ActionResult EditReservation(int currentRes)
		{
			int currentResInt = Convert.ToInt32(currentRes);
			ReservationPageViewModel resVM = new ReservationPageViewModel();
			reservation findRes = new reservation();
			guest findGuest = new guest();
			ReservationType type = new ReservationType();

			findRes = db.reservations.SingleOrDefault(y => y.ID == currentRes);
			findGuest = db.guests.SingleOrDefault(i => i.ID == findRes.guestID);
			type = db.ReservationTypes.SingleOrDefault(t => t.resTypeId == findRes.typeID);

			resVM.ID = findRes.ID;
			resVM.firstName = findGuest.firstName;
			resVM.lastName = findGuest.lastName;
			resVM.checkInDate = findRes.checkInDate;
			resVM.checkOutDate = findRes.checkOutDate;
			resVM.email = findGuest.email;
			resVM.arrivalDate = findRes.arrivalDate;
			resVM.leaveDate = findRes.leaveDate;    //null
			resVM.room = findRes.room;
			resVM.isCancelled = findRes.isCancelled;
			resVM.paidDate = findRes.paidDate;
			resVM.resType = type.resType;
			resVM.noShow = findRes.noShow;

			return PartialView("_EditReservation", resVM);

		}

		// GET: Hotel/Rates
		public ActionResult Rates()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "Rates";
                return RedirectToAction("Login");
            }
            RatesPageViewModel ratesVM = new RatesPageViewModel();


			return View(ratesVM);
		}

		[HttpGet]
		public ActionResult NewRateForm()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "NewRateForm";
                return RedirectToAction("Login");
            }
            RatesPageViewModel ratesVM = new RatesPageViewModel();

			return PartialView("_NewRateForm", ratesVM);
		}

		[HttpGet]
		public ActionResult FindRateDate(System.DateTime currentDate)
		{
			try
			{
				RatesPageViewModel ratesVM = new RatesPageViewModel();
				baseRate findRate = new baseRate();
				findRate = db.baseRates.SingleOrDefault(r => r.date == currentDate);

				ratesVM.rateDate = findRate.date;
				ratesVM.rate = findRate.rate;

				return PartialView("_EditRateForm", ratesVM);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpPost]
		public ActionResult SaveNewRate(RatesPageViewModel model)
		{
			try
			{

				baseRate rateData = new baseRate();

				rateData.rate = model.rate;
				rateData.date = model.rateDate;

				db.baseRates.Add(rateData);
				db.SaveChanges();

			}
			catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("Index");
		}

		public ActionResult EditRate(RatesPageViewModel model)
		{
			try
			{
				baseRate rateData = db.baseRates.SingleOrDefault(r => r.date == model.rateDate);
				rateData.rate = model.rate;

				db.SaveChanges();
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("Index");
		}

		// GET: Hotel/CreditCard
		public ActionResult CreditCard()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "CreditCard";
                return RedirectToAction("Login");
            }
            CreditCardPageViewModel ccVM = new CreditCardPageViewModel();

			return View(ccVM);
		}

		public ActionResult FindGuest(string guestEmail)
		{
			try
			{
				CreditCardPageViewModel ccVM = new CreditCardPageViewModel();
				guest findGuest = new guest();
				creditCard findCC = new creditCard();

				findGuest = db.guests.SingleOrDefault(g => g.email == guestEmail);
				findCC = db.creditCards.SingleOrDefault(c => c.guestID == findGuest.ID);

				ccVM.gID = findGuest.ID;
				ccVM.firstName = findGuest.firstName;
				ccVM.lastName = findGuest.lastName;
				ccVM.email = findGuest.email;
				if (findCC != null)
				{
					ccVM.cID = findCC.ID;
					ccVM.guestID = findCC.guestID;
					ccVM.number = findCC.number;
					ccVM.name = findCC.name;
					ccVM.month = findCC.month;
					ccVM.year = findCC.year;
					ccVM.securityCode = findCC.securityCode;
				}

				return PartialView("_EditCreditCardForm", ccVM);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public ActionResult SaveCreditCard(CreditCardPageViewModel model)
		{
			try
			{
				CreditCardPageViewModel ccVM = new CreditCardPageViewModel();
				//guest findGuest = new guest();
				creditCard ccInfo = new creditCard();
				ccInfo = db.creditCards.SingleOrDefault(c => c.guestID == model.gID);
				bool addDB = false;
				if (ccInfo == null)
				{
					addDB = true;
					ccInfo = new creditCard();
					ccInfo.guestID = model.gID;
				}
				ccInfo.number = model.number;
				ccInfo.name = model.name;
				ccInfo.month = model.month;
				ccInfo.year = model.year;
				ccInfo.securityCode = model.securityCode;
				if (addDB)
				{
					db.creditCards.Add(ccInfo);  
				}

				db.SaveChanges();

			}
			catch (Exception ex)
			{
				throw ex;
			}

			return RedirectToAction("Index");
		}


		public ActionResult GeneratePenalties()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "GeneratePenalties";
                return RedirectToAction("Login");
            }
            GeneratePenaltiesPageViewModel gpVM = new GeneratePenaltiesPageViewModel();

			var yesterday = DateTime.Today.AddDays(-1);

			gpVM.reservations = db.reservations
				.Where(r => (r.isCancelled == false) 
					&& (r.noShow == false)
					&& (r.checkInDate == yesterday)
					&& (r.arrivalDate == null))
					.ToList();

			gpVM.guests = db.guests.ToList();

			return View(gpVM);
		}

		public ActionResult InsertPenalties()
		{
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "InsertPenalties";
                return RedirectToAction("Login");
            }
            //GeneratePenaltiesPageViewModel gpVM = new GeneratePenaltiesPageViewModel();

            var yesterday = DateTime.Today.AddDays(-1);
			reservationDay resDay;
			reservationPenalty resPen;

			List<reservation> resList = db.reservations
				   .Where(r => (r.isCancelled == false)
					   && (r.noShow == false)
					   && (r.checkInDate == yesterday)
					   && (r.arrivalDate == null))
					   .ToList();

			foreach(var item in resList)
			{
				if (db.reservationPenalties.SingleOrDefault(r => r.reservationID == item.ID) != null)
				{
					continue;
				}
				resDay = db.reservationDays.SingleOrDefault(r => r.date == yesterday 
							&& r.reservationID == item.ID);
				resPen = new reservationPenalty();
				resPen.reservationID = item.ID;
				resPen.charge = resDay.baseRate;
				db.reservationPenalties.Add(resPen);
			}

			db.SaveChanges();

			return RedirectToAction("Index");
		}

		public ActionResult getPrice(DateTime? checkIn, DateTime? checkOut, int reserveType, String fName, String lName, String email)
		{

			double mult = 0;
			double theTotal = 0;

			ReservationPageViewModel resVM = new ReservationPageViewModel();
			List<ReservationType> resList = db.ReservationTypes.ToList();
			resVM.resListItems = new SelectList(resList, "resTypeId", "resType");

			try
			{

				switch (reserveType)
				{
					case (1):
						mult = 0.75;
						break;
					case (2):
						mult = 0.8;
						break;
					case (3):
						mult = 0.85;
						break;
					case (4):
						mult = 1;
						break;
					default:
						break;
				}

				List<baseRate> rateList = db.baseRates.SqlQuery("SELECT * From baseRates Where(date >= '" + checkIn + "' AND date <= '" + checkOut + "')").ToList();

				foreach (baseRate item in rateList)
				{
					theTotal = (item.rate * mult) + theTotal;
				}
			}
			catch (Exception e)
			{
				throw e;
			}

			if (checkIn != null)
			{
				resVM.checkInDate = checkIn;
			}
			if (checkOut != null)
			{
				resVM.checkOutDate = checkOut;
			}

			resVM.resTypeId = reserveType;
			resVM.firstName = fName;
			resVM.lastName = lName;
			resVM.email = email;
			

			resVM.cost = theTotal;


			return PartialView("_ReservationForm", resVM);

		}


        // GET: Hotel/PrintBill
        public ActionResult PrintBill()
        {
            if (this.Session["CurrentUser"] == null)
            {
                TempData["LoginRedirect"] = "PrintBill";
                return RedirectToAction("Login");
            }
            PrintBillPageViewModel pbVM = new PrintBillPageViewModel();


			// Setup SelectList for DropDownBox
			//List<guest> guestList = db.guests.Where(g => g.reservations.Count > 0).ToList();
			//pbVM.guests = guestList;
			//pbVM.guestSelect = new SelectList(guestList, "ID", "email");


			return View(pbVM);
		}

        public ActionResult getBill(int thisReservation)
        {
            PrintBillPageViewModel pbVM = new PrintBillPageViewModel();


            try
            {


                reservation resData = db.reservations.SingleOrDefault(x => x.ID == thisReservation);
                ReservationType resTyData = db.ReservationTypes.SingleOrDefault(x => x.resTypeId == resData.typeID);
                guest gData = db.guests.SingleOrDefault(x => x.ID == resData.guestID);
                List<reservationDay> dayData = db.reservationDays.Where(x => x.reservationID == thisReservation).ToList();
                reservationPenalty penData = db.reservationPenalties.SingleOrDefault(x => x.reservationID == thisReservation);

                pbVM.lastName = gData.lastName;
                pbVM.firstName = gData.firstName;
                pbVM.resType = resTyData.resType;
                pbVM.room = resData.room;
                pbVM.resDayList = dayData;
                if (penData != null)
                {
                    pbVM.charge = penData.charge;
                }


                switch (resData.typeID)
                {
                    case (1):
                        pbVM.multiplier = 0.75;
                        break;
                    case (2):
                        pbVM.multiplier = 0.8;
                        break;
                    case (3):
                        pbVM.multiplier = 0.85;
                        break;
                    case (4):
                        pbVM.multiplier = 1;
                        break;
                    default:
                        break;
                }

                double sum = 0;

                foreach(var item in pbVM.resDayList)
                {
                    sum = sum + (item.baseRate * pbVM.multiplier);
                }

                pbVM.sumPrice = sum;


            }
            catch (Exception e)
            {
                throw e;
            }
            




            return PartialView("_BillTable", pbVM);
        }

		[HttpPost]
		public ActionResult UpdateBill(int resID)
		{
			int currentResInt = Convert.ToInt32(resID);
			PrintBillPageViewModel pbVM = new PrintBillPageViewModel();
			reservation res = new reservation();

			res = db.reservations.SingleOrDefault(y => y.ID == currentResInt);

			pbVM.firstName = res.guest.firstName;

			//ReservationType type = new ReservationType();
			//reservation findRes = new reservation();

			//findRes = db.reservations.SingleOrDefault(y => y.ID == guest.reservations.);

			//type = db.ReservationTypes.SingleOrDefault(t => t.resTypeId == findRes.typeID);

			/*pbVM.ID = findRes.ID;
			pbVM.firstName = findGuest.firstName;
			pbVM.lastName = findGuest.lastName;
			pbVM.checkInDate = findRes.checkInDate;
			pbVM.checkOutDate = findRes.checkOutDate;
			resVM.email = findGuest.email;
			resVM.arrivalDate = findRes.arrivalDate;
			resVM.leaveDate = findRes.leaveDate;    //null
			resVM.room = findRes.room;
			resVM.isCancelled = findRes.isCancelled;
			resVM.paidDate = findRes.paidDate;
			resVM.resType = type.resType;
			resVM.noShow = findRes.noShow;*/

			return PartialView("_BillTable", pbVM);
		}

		[HttpPost]
		public ActionResult BillableReservations(int currentGuest)
		{
			int currentGuestInt = Convert.ToInt32(currentGuest);
			PrintBillPageViewModel pbVM = new PrintBillPageViewModel();
			guest guest = new guest();
			List<reservation> resList = db.reservations.Where(r => r.guestID == currentGuestInt).ToList();
			guest = db.guests.SingleOrDefault(i => i.ID == currentGuestInt);

			pbVM.guestReservations = resList;
			pbVM.CurrentReserve = new SelectList(resList, "ID", "checkInDate");
			pbVM.currentGuest = guest;

			//ReservationType type = new ReservationType();
			//reservation findRes = new reservation();

			//findRes = db.reservations.SingleOrDefault(y => y.ID == guest.reservations.);

			//type = db.ReservationTypes.SingleOrDefault(t => t.resTypeId == findRes.typeID);

			/*pbVM.ID = findRes.ID;
			pbVM.firstName = findGuest.firstName;
			pbVM.lastName = findGuest.lastName;
			pbVM.checkInDate = findRes.checkInDate;
			pbVM.checkOutDate = findRes.checkOutDate;
			resVM.email = findGuest.email;
			resVM.arrivalDate = findRes.arrivalDate;
			resVM.leaveDate = findRes.leaveDate;    //null
			resVM.room = findRes.room;
			resVM.isCancelled = findRes.isCancelled;
			resVM.paidDate = findRes.paidDate;
			resVM.resType = type.resType;
			resVM.noShow = findRes.noShow;*/

			return PartialView("_BillReservation", pbVM);

		}
	}
}