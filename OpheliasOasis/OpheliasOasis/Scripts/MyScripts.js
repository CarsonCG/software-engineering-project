﻿$(document).ready(function () {

    

    $("#loadNewReservation").click(function (e) {
        $.ajax({


            url: "/Hotel/ReservationForm",
            type: "GET",  
            dataType: "html",
            contentType: "application/html; charset=utf-8",

            success: function (data) {
                $('#newReservation').show().html(data);
                $('#resFilter').hide();
                $('#editReservation').hide();
                $('#totCost').hide();
            },
            error: function (e) {
                alert(JSON.stringify(e));
            }
        });
    });

    $("#loadEditReservation").click(function (e) {
        $('#newReservation').hide();
        $('#resFilter').removeAttr("style").show();
        $('#editReservation').hide();
        $('#totCost').hide();
    });

    $("#reservationList").change(function (e) {
        $.ajax({

            type: 'POST',
            url: "/Hotel/EditReservation",
            data: { currentRes: $(this).val() },

            success: function (data) {
                $('#resFilter').show();
                $('#editReservation').show().html(data);
                $('#newReservation').hide();
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });
    });

    $("#printBillBtn").click(function (e) {
        $('#printBillBtn').hide();
        window.print();
        $('#printBillBtn').show();
    });

    $("#guestBillList").change(function (e) {
        $.ajax({

            type: 'POST',
            url: "/Hotel/BillableReservations",
            data: { currentGuest: $(this).val() },

            success: function (data) {
                $('#billReservation').show().html(data);
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });
    });
 
    $("#reservationBillList").change(function (e) {
        $.ajax({

            type: 'POST',
            url: "/Hotel/UpdateBill",
            data: { resID: $(this).val() },

            success: function (data) {
                $('#billTable').show().html(data);
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });
    });

    $("#loadNewRate").click(function (e) {
        $.ajax({

            url: "/Hotel/NewRateForm",
            contentType: "application/html; charset=utf-8",
            type: "GET",
            dataType: "html",

            success: function (data) {
                $('#newRate').show().html(data);
                $('#rate').val('');
                $('#rateDate').val('');
                $('#findRate').hide();
                $('#editRate').hide();
            },
            error: function (e) {
                alert('An error has occurred.');
            }
        });
    });

    $("#loadEditRate").click(function (e) {
        $('#findDate').val('');
        $('#findRate').removeAttr("hidden").show();
        $('#newRate').hide();
        $('#editRate').hide();
    });

    $("#findRateDate").click(function (e) {
        $.ajax({

            type: 'GET',
            url: "/Hotel/FindRateDate",
            data: { currentDate: $('#findDate').val() },

            success: function (data) {
                
                $('#editRate').show().html(data);
                $('#findRate').hide();
                $('#newRate').hide();
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });
    });

    $("#findGuestBtn").click(function (e) {
        $.ajax({

            type: 'GET',
            url: "/Hotel/FindGuest",
            data: {
                guestEmail: $('#findEmail').val(),
            },

            success: function (data) {

                $('#editCC').show().html(data);
                $('#findGuest').hide();
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });
    });

    $('#runBillGen').click(function (e) {
        $.ajax({

            type: 'GET',
            url: "/Hotel/getBill",
            data: {
                thisReservation: $('#thisResID').val(),
            },

            success: function (data) {
                $('#billTable').html(data);
                $('#printBill').removeAttr("style").show();
            },
            error: function (e) {
                alert('An error has occured.');
            }
        });

    });

    $('#printBill').click(function (e) {
        $('#heading').hide();
        $('#runBillGen').hide();
        $('#printBill').hide();
        $('#thisResID').hide();
        window.print();

    });
});