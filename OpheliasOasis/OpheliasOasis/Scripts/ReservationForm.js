﻿$(document).ready(function () {

    $("#otherID").change(function (e) { // post or get for sending information of 
        var hold = $(this).val();

        $.ajax({

            type: 'GET',
            url: "/Hotel/getPrice",
            data: {
                checkIn: $('#checkInDate').val(),
                checkOut: $('#checkOutDate').val(),
                reserveType: $('#otherID').val(),
                fName: $('#firstName').val(),
                lName: $('#lastName').val(),
                email: $('#email').val()
            },

            success: function (data) {
                $('#newReservation').html(data);
                $('#totCost').show();
                $('#otherID').val(hold);
                //$('#newReservation').show().html(data);

            },
            error: function (e) {
                alert('An error has occured.');
            }
        });

    });

});